package com.emathias.projecteuler;

import java.util.ArrayList;
import java.util.List;

public class Problem024 extends EulerProblem {
	
	@Override
	public String run() {
		
		int index = 1000000;
		String answer = "";
		
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
		
		while (list.size() > 0) {
			int factorial = EulerUtils.factorialSmall(list.size() - 1);
			
			double tmp = Math.ceil((double) index / (double) factorial);
			int num = (int) (tmp - 1);
			index -= num * factorial;
			answer += list.get(num);
			list.remove(num);
		}
		
		return answer;
	}
	
	@Override
	public String name() {
		return "Problem 24";
	}
	
}
