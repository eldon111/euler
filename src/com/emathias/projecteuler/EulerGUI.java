package com.emathias.projecteuler;

public class EulerGUI {
	
	public static void main(String[] args) {
		(new EulerGUI()).load();
	}
	
	public EulerGUI() {
		
	}
	
	public void load() {
		EulerProblem p = new Problem026();
		System.out.println(p.run());
	}
}
