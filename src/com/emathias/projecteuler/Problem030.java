package com.emathias.projecteuler;

public class Problem030 extends EulerProblem {
	
	@Override
	public String run() {
		
		int start = 10;
		int end = 0;
		
		for (int i = 1; i * Math.pow(9, 5) >= Math.pow(10, i); i++) {
			end = (int) Math.pow(10, i + 1);
		}
		
		int answer = 0;
		
		for (int i = start; i < end; i++) {
			int sum = 0;
			int tmp = i;
			while (tmp > 0) {
				sum += Math.pow(tmp % 10, 5);
				tmp /= 10;
			}
			if (sum == i) {
				answer += sum;
			}
		}
		
		return Integer.toString(answer);
	}
	
	@Override
	public String name() {
		return "Problem 30";
	}
	
}
