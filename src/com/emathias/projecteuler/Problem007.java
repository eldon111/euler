package com.emathias.projecteuler;

public class Problem007 extends EulerProblem {
	
	@Override
	public String run() {
		
		int primeCount = 0;
		long prime = 0;
		
		for (int num = 2; primeCount < 10001; num++) {
			if (EulerUtils.isPrime(num)) {
				primeCount++;
				prime = num;
			}
		}
		
		return Long.toString(prime);
	}
	
	@Override
	public String name() {
		return "Problem 7";
	}
}
