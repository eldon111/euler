package com.emathias.projecteuler;

public class Problem028 extends EulerProblem {
	
	@Override
	public String run() {
		
		int sum = 1; // start with 1 at the center
		int num = 1;
		for (int i = 2; i <= 1001; i += 2) {
			for (int j = 0; j < 4; j++) {
				num += i;
				sum += num;
			}
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 28";
	}
	
}
