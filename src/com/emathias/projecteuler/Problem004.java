package com.emathias.projecteuler;

public class Problem004 extends EulerProblem {
	
	@Override
	public String run() {
		
		boolean found = true;
		int answer = 0;
		
		for (int i = 999; i > 0; i--) {
			for (int j = 999; j > 0; j--) {
				String s = Integer.toString(i * j);
				found = true;
				for (int k = 0; k < Math.ceil(s.length() / 2); k++) {
					if (s.charAt(k) != s.charAt(s.length() - k - 1)) {
						found = false;
						break;
					}
				}
				if (found) {
					if (i * j > answer)
						answer = i * j;
				}
			}
		}
		
		return Integer.toString(answer);
	}
	
	@Override
	public String name() {
		return "Problem 4";
	}
	
}
