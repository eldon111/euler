package com.emathias.projecteuler;

public class Problem020 extends EulerProblem {
	
	@Override
	public String run() {
		
		int[] factorial = EulerUtils.factorial(100);
		int sum = 0;
		
		for (int i : factorial) {
			sum += i;
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 20";
	}
	
}
