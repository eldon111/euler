package com.emathias.projecteuler;

public class Problem001 extends EulerProblem {
	
	@Override
	public String run() {
		
		long startTime = getTime();
		
		int sum = 0;
		
		for (int i = 0; i < 1000; i += 3) {
			sum += i;
		}
		
		for (int i = 0; i < 1000; i += 5) {
			if (i % 3 != 0)
				sum += i;
		}
		
		printTimeSince(startTime);
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 1";
	}
}
