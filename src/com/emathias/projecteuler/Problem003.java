package com.emathias.projecteuler;

public class Problem003 extends EulerProblem {
	
	@Override
	public String run() {
		
		long start = getTime();
		
		double num = 600851475143d;
		double primeFactor = 0d;
		
//		for (int i = 1; i < Math.sqrt(num); i++) {
		{
final double num1 = num ;
final apx . lang . gen . MutableReferenceDouble primeFactor0 = new apx . lang . gen . MutableReferenceDouble (primeFactor );
{
final apx . util . RangeInteger range2 = new apx . util . RangeInteger (1 , (int) Math.sqrt(num1)); final int blockcount2 = java . lang . Runtime .getRuntime ().availableProcessors (); { final java . util . List < apx . lang . gen . Branch > branches2 =new java . util . ArrayList < apx . lang . gen . Branch > ();
final apx . lang . gen . Parallel parallelBlock2 =apx . lang . gen . Parallel .getParallelBlock ();
{
final apx . util . RangeInteger range9 = new apx . util . RangeInteger (0 , ((blockcount2 )- 1 ));
final int last5 = (range9 ). max ;
for(int block5 = (range9 ). min ;block5 <= last5 ;block5 ++ )
{
final int nbBlock2 = block5 ;
{
{
apx . lang . gen . Branch branch2 = new apx . lang . gen . Branch (){
public @java . lang .Override void run () throws java . lang . Throwable {
{
final apx . util . RangeInteger range10 = new apx . util . RangeInteger ((((range2 ). min )+ ((((range2 ). max )- ((range2 ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range2 ). max )- ((range2 ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* (nbBlock2 )), (java . lang . Math . min (((range2 ). max ), ((range2 ). min )+ ((((range2 ). max )- ((range2 ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range2 ). max )- ((range2 ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* ((nbBlock2 )+ 1 )- 1 )));
final int last6 = (range10 ). max ;
for(int block6 = (range10 ). min ;block6 <= last6 ;block6 ++ )
{
final int i = block6 ;
{
{
{
			if (num1 % i == 0) {
				if (EulerUtils.isPrime(i)) {
					primeFactor0 .ref = i;
				}
			}
		}
		
		}
}
}
}
}
}
;
branches2 .add (branch2 );
}
}
}
}
final apx . lang . gen . ExitStatus exitStatus2 =parallelBlock2 . run (branches2 );
primeFactor = primeFactor0 .ref ;
if(exitStatus2 .hasReturned ()) {
return exitStatus2 .<String >returnedValue ();
} {
final Throwable throwable2 =exitStatus2 .thrownException ();
if(throwable2 !=null) {
if(throwable2 instanceof RuntimeException ) throw (RuntimeException )throwable2 ;
if(throwable2 instanceof Error ) throw (Error )throwable2 ;
}
}
} }
}
printTimeSince(start);
		
		return Double.toString(primeFactor);
	}
	
	@Override
	public String name() {
		return "Problem 3";
	}
	
}
