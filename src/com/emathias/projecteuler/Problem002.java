package com.emathias.projecteuler;

public class Problem002 extends EulerProblem {
	
	@Override
	public String run() {
		
		int n1 = 1;
		int n2 = 2;
		int sum = 0;
		
		while (n2 < 4000000) {
			if (n2 % 2 == 0)
				sum += n2;
			
			int tmp = n2;
			n2 = n1 + n2;
			n1 = tmp;
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 2";
	}
	
}
