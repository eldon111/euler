package com.emathias.projecteuler;

import java.util.HashSet;

public class Problem029 extends EulerProblem {
	
	@Override
	public String run() {
		
		HashSet<Double> nums = new HashSet<Double>();
		
		for (int a = 2; a <= 100; a++) {
			for (int b = 2; b <= 100; b++) {
				nums.add((double) Math.pow(a, b));
			}
		}
		
		return Integer.toString(nums.size());
	}
	
	@Override
	public String name() {
		return "Problem 29";
	}
	
}
