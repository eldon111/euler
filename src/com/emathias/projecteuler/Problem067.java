package com.emathias.projecteuler;

import java.io.FileReader;
import java.util.Scanner;

public class Problem067 extends EulerProblem {
	
	@Override
	public String run() {
		
		String s = "";
		
		try {
			Scanner in = new Scanner(new FileReader("bin/ext/Problem067.txt"));
			while (in.hasNextLine()) {
				s += in.nextLine() + "\n";
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Problem018 p18 = new Problem018();
		
		return p18.computeTriangle(p18.parsePyramid(s));
	}
	
	@Override
	public String name() {
		return "Problem 67";
	}
	
}
