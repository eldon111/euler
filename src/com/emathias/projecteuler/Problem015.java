package com.emathias.projecteuler;

public class Problem015 extends EulerProblem {
	
	@Override
	public String run() {
		
		return Long.toString(numOfGridPaths(20));
	}
	
	private long numOfGridPaths(int gridSize) {
		
		long[][] tree = new long[gridSize * 2][gridSize * 2 + 1];
		tree[0][0] = tree[0][1] = 1;
		
		// expansion
		for (int i = 1; i < gridSize; i++) {
			tree[i][0] = 1;
			for (int j = 1; j < i + 1; j++) {
				tree[i][j] = tree[i - 1][j - 1] + tree[i - 1][j];
			}
			tree[i][i + 1] = 1;
		}
		
		// contraction
		for (int i = gridSize; i < gridSize * 2; i++) {
			for (int j = 0; j < gridSize * 2 - i; j++) {
				tree[i][j] = tree[i - 1][j] + tree[i - 1][j + 1];
			}
		}
		
		return tree[gridSize * 2 - 1][0];
	}
	
	@Override
	public String name() {
		return "Problem 15";
	}
	
}
