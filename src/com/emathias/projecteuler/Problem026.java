package com.emathias.projecteuler;

public class Problem026 extends EulerProblem {
	
	@Override
	public String run() {
		
		long start = getTime();
		
		int maxRepeatingDigits = 0;
		int maxRepeatingNum = 0;
		int digitsToCalc = 5000;
		
//		for (int i = 1; i < 1000; i++) {
			 {
final int digitsToCalc0 = digitsToCalc ;
final apx . lang . gen . MutableReferenceInt maxRepeatingNum0 = new apx . lang . gen . MutableReferenceInt (maxRepeatingNum );
final apx . lang . gen . MutableReferenceInt maxRepeatingDigits0 = new apx . lang . gen . MutableReferenceInt (maxRepeatingDigits );
{
final apx . util . RangeInteger range = new apx . util . RangeInteger (1 , 1000); final int blockcount = java . lang . Runtime .getRuntime ().availableProcessors (); { final java . util . List < apx . lang . gen . Branch > branches =new java . util . ArrayList < apx . lang . gen . Branch > ();
final apx . lang . gen . Parallel parallelBlock =apx . lang . gen . Parallel .getParallelBlock ();
{
final apx . util . RangeInteger range3 = new apx . util . RangeInteger (0 , ((blockcount )- 1 ));
final int last = (range3 ). max ;
for(int block = (range3 ). min ;block <= last ;block ++ )
{
final int nbBlock = block ;
{
{
apx . lang . gen . Branch branch = new apx . lang . gen . Branch (){
public @java . lang .Override void run () throws java . lang . Throwable {
{
final apx . util . RangeInteger range4 = new apx . util . RangeInteger ((((range ). min )+ ((((range ). max )- ((range ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range ). max )- ((range ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* (nbBlock )), (java . lang . Math . min (((range ). max ), ((range ). min )+ ((((range ). max )- ((range ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range ). max )- ((range ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* ((nbBlock )+ 1 )- 1 )));
final int last0 = (range4 ). max ;
for(int block0 = (range4 ). min ;block0 <= last0 ;block0 ++ )
{
final int i = block0 ;
{
{
{
			int[] num = EulerUtils.oneDividedBy(i, digitsToCalc0);
			boolean isRepeating = true;
			
			freshVariable2 : for (int j = 0; j < digitsToCalc0; j++) {
				freshVariable1 : for (int k = 1; k < (digitsToCalc0 - j) / 2; k++) {
					isRepeating = true;
					for (int l = j; l < j + k; l++) {
						freshVariable0 : for (int m = l; m < digitsToCalc0; m += k) {
							if (num[l] != num[m]) {
								isRepeating = false;
								break freshVariable0 ;
							}
						}
					}
					if (isRepeating) {
						if (k > maxRepeatingDigits0.ref ) {
							maxRepeatingDigits0 .ref = k;
							maxRepeatingNum0 .ref = i;
						}
						
						break freshVariable1 ;
					}
				}
				if (isRepeating) {
					break freshVariable2 ;
				}
			}
		}
		
		}
}
}
}
}
}
;
branches .add (branch );
}
}
}
}
final apx . lang . gen . ExitStatus exitStatus =parallelBlock . run (branches );
maxRepeatingDigits = maxRepeatingDigits0 .ref ;
maxRepeatingNum = maxRepeatingNum0 .ref ;
if(exitStatus .hasReturned ()) {
return exitStatus .<String >returnedValue ();
} {
final Throwable throwable =exitStatus .thrownException ();
if(throwable !=null) {
if(throwable instanceof RuntimeException ) throw (RuntimeException )throwable ;
if(throwable instanceof Error ) throw (Error )throwable ;
}
}
} }
}
printTimeSince(start);
		
		return Integer.toString(maxRepeatingNum);
	}
	
	@Override
	public String name() {
		return "Problem 26";
	}
	
}
