package com.emathias.projecteuler;

public class Problem012 extends EulerProblem {
	
	@Override
	public String run() {
		
		boolean done = false;
		int checkLength = 100;
		
		int sum = 0;
		for (int num = 1; !done; num++) {
			sum += num;
			
			int[] factors = EulerUtils.findFactors(sum);
			
			if (factors.length >= checkLength) {
				System.out.println(factors.length);
				checkLength += 100;
			}
			
			if (factors.length > 500)
				done = true;
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 12";
	}
	
}
