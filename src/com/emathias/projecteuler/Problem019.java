package com.emathias.projecteuler;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Problem019 extends EulerProblem {
	
	@Override
	public String run() {
		
		Calendar cal = new GregorianCalendar();
		cal.set(1901, 0, 1);
		
		Date target = new GregorianCalendar(2001, 0, 1).getTime();
		
		int count = 0;
		
		while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		
		while (cal.getTime().before(target)) {
			if (cal.get(Calendar.DAY_OF_MONTH) == 1) {
				count++;
			}
			cal.add(Calendar.WEEK_OF_MONTH, 1);
		}
		
		return Integer.toString(count);
	}
	
	@Override
	public String name() {
		return "Problem 19";
	}
	
}
