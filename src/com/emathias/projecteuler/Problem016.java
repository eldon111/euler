package com.emathias.projecteuler;

public class Problem016 extends EulerProblem {
	
	@Override
	public String run() {
		
		int curExponent = 1;
		int[] curNum = { 2 };
		
		while (curExponent < 1000) {
			int l = curNum.length;
			int[] newNum = new int[l + 1];
			
			for (int i = 0; i < l; i++) {
				// for (int j = 0; j < l; j++) {
				newNum[i] += (curNum[i] * 2) % 10;
				newNum[i + 1] += (curNum[i] * 2) / 10;
				// }
			}
			curNum = newNum;
			curExponent++;
		}
		
		int sum = 0;
		for (int i : curNum) {
			sum += i;
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 16";
	}
	
}
