package com.emathias.projecteuler;

import java.util.HashMap;

public class Problem021 extends EulerProblem {
	
	HashMap<Integer, Integer>	map	= new HashMap<Integer, Integer>(10400);
	
	@Override
	public String run() {
		
		int sum = 0;
		
		for (int i = 2; i < 10000; i++) {
			int j = getSum(i);
			if (getSum(j) == i && i != j) {
				sum += i + j;
			}
		}
		sum /= 2; // cancel duplicates
		
		return Integer.toString(sum);
	}
	
	private int getSum(int num) {
		if (map.containsKey(num)) {
			return map.get(num);
		} else {
			int[] factors = EulerUtils.findFactors(num);
			int sum = 0;
			for (int factor : factors) {
				sum += factor;
			}
			sum -= num; // my factor func. includes number itself
			map.put(num, sum);
			return sum;
		}
	}
	
	@Override
	public String name() {
		return "Problem 21";
	}
	
}
