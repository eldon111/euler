package com.emathias.projecteuler;

public class Problem014 extends EulerProblem {
	
	@Override
	public String run() {
		
		int length = 0;
		int startNum = 1;
		long[] collatz = null;
		
		for (int i = 1; i < 1000000; i++) {
			collatz = EulerUtils.findCollatzChain(i);
			if (collatz.length > length) {
				length = collatz.length;
				startNum = i;
			}
		}
		
		return "" + startNum + " | length: " + length;
	}
	
	@Override
	public String name() {
		return "Problem 14";
	}
	
}
