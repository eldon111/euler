package com.emathias.projecteuler;

import java.util.Scanner;

public class Problem022 extends EulerProblem {
	
	@Override
	public String run() {
		
		String s = "";
		try {
			Scanner in = new Scanner(getClass().getResourceAsStream(
					"/ext/Problem022.txt"));
			while (in.hasNextLine()) {
				s += in.nextLine();
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String[] names = s.replaceAll("\"", "").split(",");
		
		// for (int i = 0; i < names.length; i++) {
		// names[i] = names[i].substring(1, names[i].length() - 1);
		// }
		
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 1; i < names.length; i++) {
				if (names[i].compareTo(names[i - 1]) < 0) {
					String tmp = names[i - 1];
					names[i - 1] = names[i];
					names[i] = tmp;
					changed = true;
				}
			}
		}
		
		int total = 0;
		for (int i = 0; i < names.length; i++) {
			int val = 0;
			for (String letter : names[i].split("")) {
				if (letter.length() > 0)
					val += (letter.charAt(0) - 64);
			}
			total += (val * (i + 1));
		}
		
		return Integer.toString(total);
	}
	
	@Override
	public String name() {
		return "Problem 22";
	}
	
}
