package com.emathias.projecteuler;

public class Problem010 extends EulerProblem {
	
	@Override
	public String run() {
		
		long start = getTime();
		
		long sum = 0;
		
//		for (int i = 2; i < 2000000; i++) {
		{
final apx . lang . gen . MutableReferenceLong sum0 = new apx . lang . gen . MutableReferenceLong (sum );
{
final apx . util . RangeInteger range0 = new apx . util . RangeInteger (2 , 2000000); final int blockcount0 = java . lang . Runtime .getRuntime ().availableProcessors (); { final java . util . List < apx . lang . gen . Branch > branches0 =new java . util . ArrayList < apx . lang . gen . Branch > ();
final apx . lang . gen . Parallel parallelBlock0 =apx . lang . gen . Parallel .getParallelBlock ();
{
final apx . util . RangeInteger range5 = new apx . util . RangeInteger (0 , ((blockcount0 )- 1 ));
final int last1 = (range5 ). max ;
for(int block1 = (range5 ). min ;block1 <= last1 ;block1 ++ )
{
final int nbBlock0 = block1 ;
{
{
apx . lang . gen . Branch branch0 = new apx . lang . gen . Branch (){
public @java . lang .Override void run () throws java . lang . Throwable {
{
final apx . util . RangeInteger range6 = new apx . util . RangeInteger ((((range0 ). min )+ ((((range0 ). max )- ((range0 ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range0 ). max )- ((range0 ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* (nbBlock0 )), (java . lang . Math . min (((range0 ). max ), ((range0 ). min )+ ((((range0 ). max )- ((range0 ). min )+ 1 )/ (java . lang . Runtime .getRuntime ().availableProcessors ())+ ((((range0 ). max )- ((range0 ). min )+ 1 )% (java . lang . Runtime .getRuntime ().availableProcessors ())== 0 ? 0 : 1 ))* ((nbBlock0 )+ 1 )- 1 )));
final int last2 = (range6 ). max ;
for(int block2 = (range6 ). min ;block2 <= last2 ;block2 ++ )
{
final int i = block2 ;
{
{
{
			if (EulerUtils.isPrime(i)) {
				sum0 .ref += i;
			}
		}
		
		}
}
}
}
}
}
;
branches0 .add (branch0 );
}
}
}
}
final apx . lang . gen . ExitStatus exitStatus0 =parallelBlock0 . run (branches0 );
sum = sum0 .ref ;
if(exitStatus0 .hasReturned ()) {
return exitStatus0 .<String >returnedValue ();
} {
final Throwable throwable0 =exitStatus0 .thrownException ();
if(throwable0 !=null) {
if(throwable0 instanceof RuntimeException ) throw (RuntimeException )throwable0 ;
if(throwable0 instanceof Error ) throw (Error )throwable0 ;
}
}
} }
}
printTimeSince(start);
		
		return Long.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 10";
	}
	
}
