package com.emathias.projecteuler;

import java.util.ArrayList;
import java.util.List;

public class Problem027 extends EulerProblem {
	
	@Override
	public String run() {
		// generate a list of primes < 1000
		// shouldn't need anything under 80, since we already have
		// an equation for 80 consecutive primes
		List<Integer> primes = new ArrayList<Integer>(100);
		for (int i = 80; i < 1000; i++) {
			if (EulerUtils.isPrime(i)) {
				primes.add(i);
			}
		}
		
		int maxPrimes = 0, maxA = 0, maxB = 0;
		for (int a = -999; a < 1000; a++) {
			for (int b : primes) {
				for (int n = 1; n < b; n++) {
					if (!EulerUtils.isPrime(n * n + a * n + b)) {
						if (n > maxPrimes) {
							maxPrimes = n;
							maxA = a;
							maxB = b;
						}
						break;
					}
				}
			}
		}
		
		System.out.println("a = " + maxA + ", b = " + maxB + ", primes = "
				+ maxPrimes);
		return Integer.toString(maxA * maxB);
	}
	
	@Override
	public String name() {
		return "Problem 27";
	}
}
