package com.emathias.projecteuler;

public class Problem009 extends EulerProblem {
	
	@Override
	public String run() {
		
		// int limit = (int) (Math.sqrt(1000) / 2);
		int limit = 500;
		
		System.out.println(2356 % 1);
		
		for (int i = 1; i < limit; i++) {
			for (int j = i; j < limit; j++) {
				double k = Math.sqrt(i * i + j * j);
				if (k % 1 == 0) {
					if (i + j + k == 1000) {
						System.out.println("a=" + i + ";b=" + j + ";c=" + k);
						return Double.toString(i * j * k);
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public String name() {
		return "Problem 9";
	}
	
}
