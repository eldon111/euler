package com.emathias.projecteuler;

public abstract class EulerProblem {
	
	// return answers as strings, for flexibility
	public abstract String run();
	
	public abstract String name();
	
	protected long getTime() {
		return System.nanoTime();
	}
	
	protected long getTimeSince(long start) {
		return System.nanoTime() - start;
	}
	
	protected void printTimeSince(long start) {
		System.out.println("Time: " + ((System.nanoTime() - start) / 1000000)
				+ "ms");
	}
}
