package com.emathias.projecteuler;

public class Problem006 extends EulerProblem {
	
	@Override
	public String run() {
		
		long sum = 0;
		long square = 0;
		
		for (int i = 1; i <= 100; i++) {
			sum += (i * i);
			square += i;
		}
		square = square * square;
		
		return Long.toString(square - sum);
	}
	
	@Override
	public String name() {
		return "Problem 6";
	}
	
}
