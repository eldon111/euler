package com.emathias.projecteuler;

public class Problem017 extends EulerProblem {
	
	@Override
	public String run() {
		
		final int AND = 3;
		final int HUNDRED = 7;
		
		final int ONES = 0;
		final int TENS = 1;
		final int TEENS = 2;
		
		int sum = 0;
		
		int[][] lengths = { { 0, 3, 3, 5, 4, 4, 3, 5, 5, 4 },
				{ 0, 0, 6, 6, 5, 5, 5, 7, 6, 6 },
				{ 3, 6, 6, 8, 8, 7, 7, 9, 8, 8 } };
		
		for (int i = 1; i < 1000; i++) {
			int num = i;
			
			if (num > 99) {
				int tmp = num / 100;
				sum += lengths[ONES][tmp];
				sum += HUNDRED;
				
				num -= (tmp * 100);
				
				if (num > 0)
					sum += AND;
			}
			
			if (num > 19) {
				int tmp = num / 10;
				sum += lengths[TENS][tmp];
				num -= (tmp * 10);
			}
			
			if (num > 9) {
				sum += lengths[TEENS][num % 10];
				num = 0;
			}
			
			sum += lengths[ONES][num];
		}
		sum += 11; // for the special case: 'one thousand'
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 17";
	}
	
}
