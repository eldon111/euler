package com.emathias.projecteuler;

import java.util.ArrayList;
import java.util.List;

public class Problem023 extends EulerProblem {
	
	@Override
	public String run() {
		
		List<Integer> abundants = new ArrayList<Integer>();
		
		int sum = 0;
		
		for (int i = 1; i <= 28123; i++) {
			if (EulerUtils.isPerfect(i) > 0) {
				abundants.add(i);
			}
			
			boolean add = true;
			for (int j = 0; j < abundants.size(); j++) {
				if (!add) {
					break;
				}
				for (int k = j; k < abundants.size(); k++) {
					if (abundants.get(j) + abundants.get(k) > i) {
						break;
					}
					if (abundants.get(j) + abundants.get(k) == i) {
						add = false;
						break;
					}
				}
			}
			if (add) {
				sum += i;
			}
		}
		
		return Integer.toString(sum);
	}
	
	@Override
	public String name() {
		return "Problem 23";
	}
	
}
