package com.emathias.projecteuler;

public class Problem031 extends EulerProblem {
	
	@Override
	public String run() {
		
		int[] denoms = { 1, 2, 5, 10, 20, 50, 100, 200 };
		
		int num = 10;
		int total = 1;
		
		int d1, d2; // denom pointers
		
		for (d1 = 1; denoms[d1] <= num; d1++) {
			int tmp = num;
			for (d2 = d1; tmp > 1; d2--) {
				total += tmp / denoms[d2];
				tmp -= (denoms[d2] * (tmp / denoms[d2]));
			}
		}
		
		return Integer.toString(total);
	}
	
	// private int
	
	@Override
	public String name() {
		return "Problem 31";
	}
}
