package com.emathias.projecteuler;

public class Problem005 extends EulerProblem {
	
	@Override
	public String run() {
		
		boolean found = false;
		
		for (int num = 1; found == false; num++) {
			found = true;
			for (int j = 1; j <= 20; j++) {
				if (num % j != 0) {
					found = false;
					break;
				}
			}
			if (found)
				return Integer.toString(num);
		}
		
		return null;
	}
	
	@Override
	public String name() {
		return "Problem 5";
	}
	
}
